﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplicationPerfil.Modelo
{
    class Ordem
    {
        private Random RandNum = new Random();
        private int qtdJogadores = new int();
        private List<int> Sequencia = new List<int>();

        public  Ordem(int qtdJogadores){
            this.qtdJogadores  = qtdJogadores;
            int i = 1;
            while (i <= qtdJogadores)
            {
                int num = RandNum.Next(1, (qtdJogadores+1));
                if (!Sequencia.Contains(num))
                {
                  Sequencia.Add(num);
                  i++;
                }
            }
        }

        public List<int> GetOrdem()
        {
            return Sequencia;
        }

    }
}
