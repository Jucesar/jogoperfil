﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplicationPerfil.Negócio
{
    class NPartida
    {
        private List<Participante> participantes;
        private string nomes;
        private int i;
        private int count = 0;
        private string vencedor;

        public void Contagem(List<Participante> participantes, int quantidade)
        {
            this.participantes = participantes;
            int i = 0;
            while (i < quantidade)
            {
                if (i >= 1) this.nomes += ", ";
                this.nomes += participantes[i].ToString();
                i++;
            }
        }

        public List<Participante> Embaralha()
        {
            Random rand = new Random();
            foreach (Participante part in participantes)
            {
                i++;
            }
            for (int a = 0; a < 100; a++)
            {
                int c = rand.Next(i);
                int b = rand.Next(i);
                Participante part = participantes[c];
                participantes[c] = participantes[b];
                participantes[b] = part;

            }
            return participantes;
        }

        public void ComecaRodada()
        {
            var Rodada = new WindowRodada(participantes, count);
            Rodada.ShowDialog();
            count++;
        }

        public string Nomes()
        {
            return nomes;
        }

        public void definirPapeis()
        {
            participantes [count].SetJogador(true);
            participantes[count + 1].SetMediador(true);
        }   

        public string nomeJogador()
        {
            return participantes[count].ToString(); 
        }

        public string nomeMediador()
        {
            return participantes[count + 1].ToString();
        }

    }
}
