﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplicationPerfil.Negócio
{
    class NRodada
    {
          private List<Participante> jogadores;
          private int ordem;
          private int dicas_usadas = 0;
          private List<string> Perfis = new List<string>();
          private int vez = 0;
          private int mediador = 0;
         
        public NRodada(List<Participante> jogadores, int ordem)
        {
            this.jogadores = jogadores;
            this.ordem = ordem;
            mediador = ordem + 1;
        }

        public string nomeJogador()
        {
            return "Jogador da vez: " + jogadores[ordem].ToString();
        }

        public string nomeMediador()
        {
            return "Mediador da vez: " + jogadores[ordem + 1].ToString();
        }

        public string JogadorVez()
        {
            vez = vez + 1;
            if (vez == mediador)
            {
                vez = vez + 1;
            }
            if (vez >= jogadores.Count)
            {
                vez = ordem;
            }
            return "Jogador da vez: " + jogadores[vez];
        }

        
        public void LePerfil()
        {
            string linha;
            StreamReader sr = new StreamReader("..\\..\\Negócio\\Perfis.txt");
            try
            {
                while (sr.Peek() > -1)
                {
                    linha = sr.ReadLine();
                    Perfis.Add(linha);
                }
            }
            finally
            {
                sr.Close();
            }
        }
        public List<string> GetPerfil()
        {
            return Perfis;
        }

        public void usaDica()
        {
            dicas_usadas = dicas_usadas + 1;
        }
        public int getDicasUsadas()
        {
            return dicas_usadas;
        }

    }
}
