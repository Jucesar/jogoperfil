﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApplicationPerfil
{
    /// <summary>
    /// Lógica interna para WindowCadastroNomes2.xaml
    /// </summary>
    public partial class WindowCadastroNomes2 : Window
    {
        public WindowCadastroNomes2()
        {
            InitializeComponent();
        }

        private List<String> Nomes = new List<String>();

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Nomes.Add(TextboxNome1.Text);
            Nomes.Add(TextboxNome2.Text);
            this.Close();
        }

        public List<String> GetNomes()
        {
            return Nomes;
        }
    }
}
