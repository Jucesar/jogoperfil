﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfApplicationPerfil.Negócio;

namespace WpfApplicationPerfil
{
    /// <summary>
    /// Lógica interna para WindowRodada.xaml
    /// </summary>
    public partial class WindowRodada : Window
    {
        NRodada Nrod;
        private List<Participante> jogadores;
        private int ordem;
        private int indice = 2;

        public WindowRodada(List<Participante> jogadores, int ordem)
        {
            InitializeComponent();
            Nrod = new NRodada(jogadores, ordem);
            jogadorVez.Content = Nrod.nomeJogador();
            mediadorVez.Content = Nrod.nomeMediador();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Nrod.LePerfil();
            Dicas();
        }

     

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Nrod.usaDica();
            dicasUsadas.Content = Nrod.getDicasUsadas();
            jogadorVez.Content = Nrod.JogadorVez();
            if (Nrod.getDicasUsadas() >= 10)
            {
                MessageBox.Show("Mediador venceu!");
            }
        }


        void Dicas()
        {
            string[] perfil;
            foreach (string linha in Nrod.GetPerfil())
            {
                perfil = linha.Split(';');
                if (perfil[0] == indice.ToString())
                {
                    dica1.Content += perfil[3];
                    dica2.Content += perfil[4];
                    dica3.Content += perfil[5];
                    dica4.Content += perfil[6];
                    dica5.Content += perfil[7];
                    dica6.Content += perfil[8];
                    dica7.Content += perfil[9];
                    dica8.Content += perfil[10];
                    dica9.Content += perfil[11];
                    dica10.Content += perfil[12];
                    categoriaPerfil.Content += perfil[2];
                    perfilJogador.Content += perfil[1];
                }
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Jogador da vez venceu!");
        }
    }
}
