﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfApplicationPerfil.Negócio;

namespace WpfApplicationPerfil
{
    /// <summary>
    /// Lógica interna para WindowRodadaManager.xaml
    /// </summary>
    public partial class WindowRodadaManager : Window
    {
        NPartida Npart = new NPartida();

        public WindowRodadaManager(List<Participante> participantes, int quantidade)
        {
            Npart.Contagem(participantes, quantidade);

            InitializeComponent();

            partPartida.Text += Npart.Nomes();
            Npart.definirPapeis();
            JogadorVez.Content += Npart.nomeJogador();
            MediadorVez.Content += Npart.nomeMediador();
        }

        private List<Participante> Ordenar()
        {
            return Npart.Embaralha();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Npart.ComecaRodada();
        }
    }
}
